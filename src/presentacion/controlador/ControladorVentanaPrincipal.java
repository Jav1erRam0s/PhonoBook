package presentacion.controlador;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import dto.ContactoDTO;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import modelo.GestorContactos;
import utils.ImportExportBD;

public class ControladorVentanaPrincipal 
{
	@FXML
	public BorderPane borderPaneVentanaPrincipal;
	
	@FXML
	public Button btnAgregar;
	@FXML
	public Button btnEditar;
	@FXML
	public Button btnEliminar;

	@FXML
	public TableView<ContactoDTO> tblContactos;
	@FXML
	private TableColumn<ContactoDTO, String> colNombre;
	@FXML
	private TableColumn<ContactoDTO, String> colTelefono;
	@FXML
	private TableColumn<ContactoDTO, String> colDireccion;
	@FXML
	private TableColumn<ContactoDTO, String> colReferencia;
	
	public Tooltip tooltip = new Tooltip();
	
	@FXML
	public TextField txtFiltro;

	@FXML
	public MenuButton MenuButtonBD;
	
	public ObservableList<ContactoDTO> contactos;
	public ObservableList<ContactoDTO> contactosFiltrados;

	@FXML
	private HBox hBoxBuscador;
	@FXML
	private HBox hBoxActions;
	@FXML
	private Label lblBuscador;

	public void initialize()
	{	
		Image img1 = new Image("resources/images/buscador.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(25);
	    view1.setFitWidth(25);
	    this.lblBuscador.setGraphic(view1);
		
	    Image img2 = new Image("resources/images/agregar.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(17);
	    view2.setFitWidth(17);
	    this.btnAgregar.setGraphic(view2);
	    
	    Image img3 = new Image("resources/images/editar.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(17);
	    view3.setFitWidth(17);
	    this.btnEditar.setGraphic(view3);
	    
	    Image img4 = new Image("resources/images/eliminar.png");
	    ImageView view4 = new ImageView(img4);
	    view4.setFitHeight(17);
	    view4.setFitWidth(17);
	    this.btnEliminar.setGraphic(view4);

		Image img5 = new Image("resources/images/bd.png");
	    ImageView view5 = new ImageView(img5);
	    view5.setFitHeight(17);
	    view5.setFitWidth(17);
		this.MenuButtonBD.setGraphic(view5);
		
		this.txtFiltro.setStyle("-fx-text-inner-color : orange; -fx-background-color: black; ");
		
	    this.borderPaneVentanaPrincipal.setStyle(" -fx-background-image: url('resources/images/fondo.jpg'); ");  
		this.tooltip.setStyle("-fx-text-fill: orange;");	    
		
		this.colNombre.prefWidthProperty().bind( this.tblContactos.widthProperty().multiply(.25) );
		this.colTelefono.prefWidthProperty().bind( this.tblContactos.widthProperty().multiply(.23) );
		this.colDireccion.prefWidthProperty().bind( this.tblContactos.widthProperty().multiply(.3) );
		this.colReferencia.prefWidthProperty().bind( this.tblContactos.widthProperty().multiply(.2) );

		this.colNombre.setResizable(false);
		this.colTelefono.setResizable(false);
		this.colDireccion.setResizable(false);
		this.colReferencia.setResizable(false);

		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);
		
		this.contactos = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
		this.contactosFiltrados = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
		this.tblContactos.setItems(this.contactos);

		this.colNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		this.colTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));
		this.colDireccion.setCellValueFactory(new PropertyValueFactory<>("direccion"));
		this.colReferencia.setCellValueFactory(new PropertyValueFactory<>("referencia"));
		
		this.colNombre.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colTelefono.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colDireccion.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colReferencia.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		
		Platform.runLater( () -> this.tblContactos.scrollTo(this.contactos.size()-1) );
		
		MenuItem menuItemImport = new MenuItem("Importar");
		MenuItem menuItemExport = new MenuItem("Exportar");
		menuItemImport.setOnAction(e -> this.importar());
		menuItemExport.setOnAction(e -> this.exportar());
		this.MenuButtonBD.getItems().addAll( menuItemImport, menuItemExport );
	}	
	
	@FXML
	public void agregarContacto(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaAgregarEditarContacto.fxml"));
		
		try 
		{
			Parent root = loader.load();
			
			ControladorVentanaAgregarEditarContacto contro = loader.getController();
			contro.initialize("agregar", null, this);

			this.borderPaneVentanaPrincipal.setLeft(root);			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	@FXML
	public void editarContacto(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaAgregarEditarContacto.fxml"));
		
		try 
		{
			Parent root = loader.load();
			
			ControladorVentanaAgregarEditarContacto contro = loader.getController();
			contro.initialize("editar", this.tblContactos.getSelectionModel().getSelectedItem(), this);

			this.borderPaneVentanaPrincipal.setLeft(root);			
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}	
	}
	
	@FXML
	public void eliminarContacto(ActionEvent event) 
	{
	    if (this.mostrarAlertConfirmation(null).get() == ButtonType.OK)
	    {
			GestorContactos.getInstance().delete(this.tblContactos.getSelectionModel().getSelectedItem());
			this.contactosFiltrados.remove(this.tblContactos.getSelectionModel().getSelectedItem());
			this.contactos.remove(this.tblContactos.getSelectionModel().getSelectedItem());
			this.tooltip.setText(null);
	    }
	}

	@FXML
	public void habilitarAcciones(MouseEvent event) 
	{
		if( this.tblContactos.getSelectionModel().getSelectedIndex() != -1 )
		{
			this.borderPaneVentanaPrincipal.setLeft(null);
			this.btnAgregar.setDisable(false);
			this.btnEditar.setDisable(false);
			this.btnEliminar.setDisable(false);
			this.MenuButtonBD.setDisable(false);
		}
		else
		{
			this.btnEditar.setDisable(true);
			this.btnEliminar.setDisable(true);
		}
	}
	
    public void importar() 
	{
		FileChooser select = new FileChooser();
		select.setTitle("Importar base de datos");
		String user = System.getProperty("user.name");
		File defaultDirectory = new File("C:/Users/"+user+"/Downloads");
		select.setInitialDirectory(defaultDirectory);
		select.getExtensionFilters().addAll( new FileChooser.ExtensionFilter("SQL", "*.sql") );

		Stage newStage = new Stage();

		File file = select.showOpenDialog( newStage );

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        newStage.setX((screenBounds.getWidth() - newStage.getWidth()) / 2);
        newStage.setY((screenBounds.getHeight() - newStage.getHeight()) / 2);

		if (file != null) 
		{
            String fileAsString = file.toString();
            if( ImportExportBD.Importar( fileAsString ) )
            {

    			this.mostrarAlertInformacion("Importacion Exitosa!!");
    			this.contactos.clear();
    			this.contactos = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
    			this.contactosFiltrados.clear();
    			this.contactosFiltrados = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
    			this.tblContactos.setItems(this.contactosFiltrados);
    			this.tblContactos.refresh();
            }
            else
            {
    			this.mostrarAlertError("Ocurrio un error al importar");
            }
        }
	}

    public void exportar() 
	{
		DirectoryChooser select = new DirectoryChooser ();
		select.setTitle("Exportar base de datos");
		String user = System.getProperty("user.name");
		File defaultDirectory = new File("C:/Users/"+user+"/Downloads");
		select.setInitialDirectory(defaultDirectory);
		
		Stage newStage = new Stage();
		File selectedDirectory = select.showDialog(newStage);

        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        newStage.setX((screenBounds.getWidth() - newStage.getWidth()) / 2);
        newStage.setY((screenBounds.getHeight() - newStage.getHeight()) / 2);
		
		if (selectedDirectory != null) 
		{
            String fileAsString = selectedDirectory.toString();
            if( ImportExportBD.Exportar( fileAsString ) )
            {
    			this.mostrarAlertInformacion("Exportacion Exitosa!!");            	
            }
            else
            {
    			this.mostrarAlertError("Ocurrio un error al exportar");
            }
        }
	}

	@FXML
	public void filtrar(KeyEvent event) 
	{
		this.btnEditar.setDisable(true);
		this.btnEliminar.setDisable(true);
		
		String nombre = this.txtFiltro.getText().toLowerCase();
		
		if( nombre.isEmpty() )
		{
			this.contactosFiltrados.clear();
			this.contactosFiltrados = FXCollections.observableArrayList( GestorContactos.getInstance().readAll() );
			
			this.tblContactos.setItems(this.contactosFiltrados);
			this.tblContactos.refresh();
		}
		else
		{
			this.contactosFiltrados.clear();
			for(ContactoDTO p : this.contactos)
			{
				String nameAndRef = p.getNombre() + " " + p.getReferencia();
				if( nameAndRef.toLowerCase().contains(nombre) )
				{
					this.contactosFiltrados.add(p);
				}
			}
			this.tblContactos.setItems(this.contactosFiltrados);	
			this.tblContactos.refresh();
		}
	}
	
	@FXML
	public void verPopUpMouse(MouseEvent event) 
	{
		this.mostrarPopUp();
	}

	@FXML
	public void verPopUpKey(KeyEvent event) 
	{
		this.mostrarPopUp();
	}
	
	private void mostrarPopUp()
	{
		if( this.tblContactos.getSelectionModel().getSelectedIndex() != -1 )
		{
			String mensaje = "";
			if(	this.tblContactos.getSelectionModel().getSelectedItem().getDescripcion().length() != 0 )
			{ mensaje = mensaje + this.tblContactos.getSelectionModel().getSelectedItem().getDescripcion(); }
			if(	this.tblContactos.getSelectionModel().getSelectedItem().getOtrosmedios().length() != 0 
				&& mensaje.length() != 0)
			{ mensaje = mensaje + "\n" + "---" + "\n" + this.tblContactos.getSelectionModel().getSelectedItem().getOtrosmedios(); }
			if( (this.tblContactos.getSelectionModel().getSelectedItem().getOtrosmedios() != null 
				|| this.tblContactos.getSelectionModel().getSelectedItem().getOtrosmedios().length() != 0 )
				&& mensaje.length() == 0)
			{ mensaje = mensaje + this.tblContactos.getSelectionModel().getSelectedItem().getOtrosmedios(); }
			if( mensaje.length() == 0) { this.tooltip.setText("Empty"); }
			else { this.tooltip.setText(mensaje); }
			this.tblContactos.setTooltip(tooltip);	
		}	
	}

	private void mostrarAlertInformacion(String mensaje) 
	{
	    Alert alert = new Alert(Alert.AlertType.INFORMATION);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/phonobook.png"));
	    alert.setTitle("Info");
	    alert.setContentText( mensaje );
	    alert.showAndWait();
	}

	private Optional<ButtonType> mostrarAlertConfirmation(ActionEvent event) 
	{
	    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/phonobook.png"));
	    alert.setTitle("Confirmacion");
	    alert.setContentText("�Deseas realmente eliminar?");
	    return alert.showAndWait();
	}
	
	private void mostrarAlertError(String mensaje) 
	{
	    Alert alert = new Alert(Alert.AlertType.ERROR);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/phonobook.png"));
	    alert.setTitle("Error");
	    alert.setContentText( mensaje );
	    alert.showAndWait();
	}
	
}