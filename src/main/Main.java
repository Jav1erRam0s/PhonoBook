package main;

import java.io.IOException;
import java.net.ServerSocket;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main  extends Application 
{

	public ServerSocket SERVER_SOCKET;

	@Override
	public void start(Stage primaryStage) 
	{
		try 
		{	
			SERVER_SOCKET = new ServerSocket(20211); // a�o+app = 20211
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/presentacion/vista/VentanaPrincipal.fxml"));
			
			Pane ventana = (Pane) loader.load();
			
			Scene scene = new Scene(ventana, 800, 475);
			primaryStage.setScene(scene);
			
			primaryStage.getIcons().add(new Image("/resources/images/phonobook.png"));
			primaryStage.setTitle("PhonoBook");
			
			primaryStage.setResizable(false);
			primaryStage.show();
			
			// Cerramos el socket al cerrar la ventana.
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent t) 
			    {
			    	try 
			    	{
						SERVER_SOCKET.close();
						System.exit(0);
					} 
			    	catch (IOException e) 
			    	{
			    		System.out.println( e.getMessage() );
					}
			    }
			});
			
	        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	        primaryStage.setX((screenBounds.getWidth() - primaryStage.getWidth()) / 2);
	        primaryStage.setY((screenBounds.getHeight() - primaryStage.getHeight()) / 2);
		} 
		catch(Exception e) 
		{
    		System.out.println( e.getMessage() );
		}
	}
	
	public static void main(String[] args) 
	{
		launch(args);
	}

}