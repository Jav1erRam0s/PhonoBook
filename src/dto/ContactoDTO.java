package dto;

public class ContactoDTO 
{

	private String idContacto;
	private String nombre;
	private String descripcion;
	private String telefono;
	private String direccion;
	private String otrosmedios;
	private String referencia;

	public String getIdContacto() 
	{
		return idContacto;
	}

	public void setIdContacto(String idContacto) 
	{
		this.idContacto = idContacto;
	}

	public String getNombre() 
	{
		return nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}
	
	public String getDescripcion() 
	{
		return descripcion;
	}

	public void setDescripcion(String descripcion) 
	{
		this.descripcion = descripcion;
	}

	public String getTelefono() 
	{
		return telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	
	public String getDireccion() 
	{
		return direccion;
	}

	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}
	public String getOtrosmedios() 
	{
		return otrosmedios;
	}

	public void setOtrosmedios(String otrosmedios) 
	{
		this.otrosmedios = otrosmedios;
	}

	public String getReferencia() 
	{
		return referencia;
	}

	public void setReferencia(String referencia) 
	{
		this.referencia = referencia;
	}
	
}