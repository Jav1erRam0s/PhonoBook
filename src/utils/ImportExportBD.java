package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import persistencia.conexion.Conexion;

public class ImportExportBD 
{

	static Conexion Connection = Conexion.getConexion();
	
	public static boolean Exportar( String rutaExport )
	{
		String ruta = rutaExport;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String comando = "";
	
		if(ruta.trim().length()!=0) 
		{
			try 
			{
				String nombre = "\\"+Connection.getBd()+"."+dtf.format(LocalDateTime.now())+".sql";
				
				// Base de Datos - Local
				comando = "C:\\Program Files\\MariaDB 10.5\\bin\\mysqldump --opt -u"+Connection.getUser()+" -p"+Connection.getPass()+" -B "+Connection.getBd()+" -r "+ruta+nombre;
				// Base de datos - Instalador
//				comando = "C:\\ProgramData\\PhonoBook\\bin\\mysqldump --opt -u"+Connection.getUser()+" -p"+Connection.getPass()+" -B "+Connection.getBd()+" -r "+ruta+nombre;
				
				Runtime rt = Runtime.getRuntime();
				rt.exec(comando);
				return true;
			}
			catch(Exception ex) 
			{
				System.out.print( "Error : " + ex.getMessage() );
			}
		}		
		return false;
	}
	
	public static boolean Importar( String rutaImport ) 
	{
		String ruta = rutaImport;
		String comando = "";
		if(ruta.trim().length()!=0) 
		{
			try 
			{
				// Base de Datos - Local
				comando = "cmd /c \"C:\\Program Files\\MariaDB 10.5\\bin\\mysql.exe\""+" -u"+Connection.getUser()+" -p"+Connection.getPass()+" "+Connection.getBd()+" < "+ruta;
				// Base de datos - Instalador
//				comando = "cmd /c \"C:\\ProgramData\\PhonoBook\\bin\\mysql.exe\""+" -u"+Connection.getUser()+" -p"+Connection.getPass()+" < "+ruta;
				Runtime rt = Runtime.getRuntime();
				rt.exec(comando);
				return true;
			}
			catch(Exception ex) 
			{
				System.out.print( "Error : " + ex.getMessage() );
			}
		}		
		return false;
		
	}
	
}